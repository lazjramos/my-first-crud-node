require('../models/Element');
const mongoose = require('mongoose');
const Element = mongoose.model('Element');
const assert = require('assert');

module.exports = {
    index: async (req, res) => {
        var errors = req.session.errors;
        var success = req.session.success;
      
        var docs = await Element.find();
        res.render('index',{items: doc, errors: errors, success:success});

        req.session.errors = false;
        req.session.success = false;
    },
    create: (req, res) => {
        res.render('create', {title: "Crear nuevo elemento", errors: req.session.errors});
        req.session.errors = null;
        req.session.success = false;
    },
    edit: async (req, res) => {
        var id = req.params.id;
        
        var doc = await Element.findById(id);
        res.render('edit',{element: doc, title:'Editar elemento'});
    },
    store: async (req, res) => {
        req.assert('title', 'El titulo es requerido').notEmpty();
        req.assert('description', 'La descripcion es requerida').notEmpty();
        req.assert('description', 'Debe tener entre 6 y 20 caracteres').len(6, 20);
      
        req.getValidationResult().then((result)=>{
          if(!result.isEmpty()){
            req.session.errors = result.array();
            req.session.success = false;
          }
          else{
            item = {
              title: req.body.title,
              description: req.body.description
            }
      
            // Saving in the DDBB
            var data = new Element(item);
            data.save();
      
            req.session.success = 'true';
            req.session.errors = false;
          }
          res.redirect('/');
        })
    },
    update: (req, res) => {
      var id = req.body.id;
      
        Element.findById(id,(err,doc)=>{
          if(err){
            console.log(err)
          }
          doc.title = req.body.title;
          doc.description = req.body.description;
          doc.save();
          res.redirect('/');
        });
    },
    delete: async (req, res) => {
      var id = req.body.id;
      await Element.findByIdAndRemove(id).exec();
      res.redirect('/');  
    }
}