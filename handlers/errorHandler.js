// Handle errors with async/await functions
module.exports = {
    catchErrors : (fn) => {
        return function(req, res , next) {
            return fn(req, res, next). catch(next);
        }
    }
}