const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

// Setting up the connection through mongoose
mongoose.connect('localhost:27017/test');

const elementSchema = new mongoose.Schema({
    title: {type: String, trim: true, required:'El campo titulo es requerido'},
    description: String
});

module.exports = mongoose.model('Element', elementSchema);