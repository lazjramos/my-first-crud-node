const express = require('express');
const router = express.Router();
const elementController = require('../controllers/elementController');
const {catchErrors} = require('../handlers/errorHandler');

/* GET Index page. */
router.get('/', catchErrors(elementController.index));

/* Get create page */
router.get('/create', elementController.create);
 
/* Get edit page and an element to edit */
router.get('/edit/:id', catchErrors(elementController.edit));

/*Post to store a new element */
router.post('/store', elementController.store);

/*Post to update an element */
router.post('/update', elementController.update);

/*Post to delete an element */
router.post('/delete', elementController.delete);

module.exports = router;
